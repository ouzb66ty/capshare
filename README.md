# capshare

Projet de développement d'un réseau de partage de fichiers peer-to-peer via IPFS en Javascript.

## Description

Le principe est de développer une alternative open source et libre à Soulseek basé sur le protocole décentralisé IPFS.

Ce réseau doit être constitué d'un serveur central pour permettre de rechercher des fichiers/dossiers depuis un ensemble de dépôts IPFS (dossiers de partage).

Le client est un gestionnaire des dossiers de partage pour les synchroniser et communiquer avec l'API du serveur central, le téléchargement en P2P avec tous les atouts d'IPFS, des fonctionnalités de cryptage, de paramétrage utilisateur et de salons de discussions.

Le serveur central se charge d'enregistrer les données concernant les utilisateurs. Les mots de passe sont hashés en Argon2, l'authentification utilise les Json Web Token avec un système de refresh des tokens toutes les 15 minutes via l'API. Ce qui fait le réseau Capshare est en partie l'enregistrement en base données des chemins IPFS permettant ainsi au serveur central de lancer des recherches globales via la commande _ls_, par exemple _/ipfs/UiH6djew87dw8eDHuew8/albums_.

## To Do

### Serveur/API REST central (api.js)

- Connexion, inscription, refresh [  OK  ]
- Mise en place de swagger [  OK  ]
- Modifier le dossier de partage [  In Progress  ]
- Rechercher un fichier/dossier sur le réseau [  In Progress  ]
- Lister fichier(s)/dossier(s) d'un utilisateur [  In Progress  ]
- Supprimer un utilisateur [  In Progress  ]
- Modifier nom d'utilisateur [  In Progress  ]
- Modifier mot de passe [  In Progress  ]
- Modifier/Supprimer/Lister les dossiers de partage [  In Progress  ]

### Client

- Intégration React/Electron/API [  In Waiting  ]
- Télécharger fichier/dossier [  In Waiting  ]

## A savoir

La documentation est accessible à l'adresse http://127.0.0.1:8001/apiDoc lorsque l'API est en marche. Vous trouverez tout ce qui (doit) concerne(r) l'installation dans le dossier _install/_ dont les fichiers mysql (mwb et sql). La base de donnée est nommée _capshare_ et elle contient la table _users_.

N'hésitez pas à me contacter pour en savoir davantages et contribuer (ensembles) si cela vous intéresse.

## Author

ouzb64ty <ouzb65ty@protonmail.ch>